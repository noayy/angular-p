import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from '../interfaces/customer';
import { TableCustomerService } from '../table-customer.service';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-leave-customer',
  templateUrl: './leave-customer.component.html',
  styleUrls: ['./leave-customer.component.css']
})
export class LeaveCustomerComponent implements OnInit {


  customers=[];
  displayedColumns: string[] = ['name','phone','email','age', 'more_than_1_card','main_city_sail', 'student', 'Utilizing_card_1', 'Utilizing_card_2','Utilizing_card_3','Utilizing_card_4','perc_private','perc_proff','predict'];
  customers$;
  new;
  resultPredict;

  lastCustomerArrived //דפדוף
  firstCustomerArrived;//דפדוף

  constructor(private TableCustomerService:TableCustomerService, private ClassifyService:ClassifyService, public authService:AuthService) { }

  // getCustomersLeave(){
  //   this.customers$ = this.TableCustomerService.getCustomersLeave(); 
  //   this.customers$.subscribe(
  //     docs =>{
  //       console.log(docs);
  //       this.customers = [];
  //       for(let document of docs){
  //        // console.log(document);
  //         const customer:Customer= document.payload.doc.data();
  //         customer.id = document.payload.doc.id; 
          
  //   //  this.TableCustomerService.updateCustomer('PFhCEC2CReMlJ1dfnESQ', 'jg');
  //       this.customers.push(customer);
  //       }
        
  //       console.log(this.customers);
  //     }
  //   )
  // }

  ngOnInit(): void {

    this.customers$ = this.TableCustomerService.getCustomersLeave();
    this.customers$.subscribe(
      docs => {  
        this.lastCustomerArrived = docs[docs.length-1].payload.doc;       
        this.customers = [];
        var i = 0;
        for (let document of docs) {
          console.log(i++); 
          const customer:Customer = document.payload.doc.data();

          customer.id = document.payload.doc.id;
             this.customers.push(customer); 
        }   
      //  console.log(this.customers);
               
      }
      
    )
}

//דפדוף
nextPage(){
this.customers$ = this.TableCustomerService.getCustomersleavenext(this.lastCustomerArrived); 
this.customers$.subscribe( 
docs => { 
this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
this.firstCustomerArrived = docs[0].payload.doc; 
this.customers = []; 
for(let document of docs){ 
  const customer:Customer = document.payload.doc.data(); 
  customer.id = document.payload.doc.id; 
  this.customers.push(customer); 
} 
}
)
}



//דפדוף
previewsPage(){
this.customers$ = this.TableCustomerService.getCustomersLeave2(this.firstCustomerArrived); 
this.customers$.subscribe( 
docs => { 
this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
this.firstCustomerArrived = docs[0].payload.doc; 
this.customers = []; 
for(let document of docs){ 
const customer:Customer = document.payload.doc.data(); 
customer.id = document.payload.doc.id; 
this.customers.push(customer); 
} 
}
)
}
  

}
