import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {


  //Api get way URL (connect to lamda function call "test")
 // private url = "https://jm4tqi1wul.execute-api.us-east-1.amazonaws.com/beta"
 private url = "https://2n661lzq2m.execute-api.us-east-1.amazonaws.com/model"
  
  classify(customers:Customer[]):Observable<any>{


      let json = {
        'data':customers
      //    [ { 
      //     'age': t,
      //     'gender' : t,
      //     'more_than_1_card': v,
      //     'main_city_sail': b,
      //     'student': c,
      //     'abord': c,
      //     'Utilizing_card_1': Utilizing_card_1,
      //     'Utilizing_card_2': Utilizing_card_2,
      //     'Utilizing_card_3': Utilizing_card_3,
      //     'Utilizing_card_4': Utilizing_card_4,
      //     'perc_private': perc_private,
      //     'perc_proff': perc_proff,
      //     'perc_cancel': c,
      //    // 'total_count':c

      //   }
      // ]
      }

      let body = JSON.stringify(json);
      console.log(body)
   return this.HttpClient.post<any>(this.url, body).pipe(
        map(res => {
          console.log(res); 
          let final:string = res.body;
          //final = final.replace('[',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
          //final = final.replace(']',''); // אנחנו רוצים להוריד את הסוגריים מהמספר שמוחזר מהבודי
          return final; 
        })
      )
  
    }


  constructor(private HttpClient:HttpClient) { }
}