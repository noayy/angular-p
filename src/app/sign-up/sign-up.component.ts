import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  //נגדיר משתנים
  hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
  email:string;
  password:string;
  errorMessage:string; //הרצאה 9
  isError:boolean = false; //הדיפולט יהיה פאלס
  
  onSubmit(){
    this.authService.SignUp(this.email,this.password)
    .then(res => 
      {
        console.log('Succesful sign up',res);
        this.router.navigate(['/welcome']); 
      }
    ).catch(
      err => {
        console.log(err); //נדפיס את התשובה לקונסול בשביל דיבאגינג
        this.isError = true; //יגרום להודעת השגיאה להיות מוצגת, לפי התנאי שרשמנו בטיימפלייט
        this.errorMessage = err.message;
      }
    );
  }

  constructor(private authService:AuthService,
    private router:Router) { }

  ngOnInit(): void {
  }

}
