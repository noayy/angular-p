import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SeaService {

  constructor(private http: HttpClient) { }


  //נוסיף משתנים
  //התחברות לקריאת האייפיאיי
  private url = "https://api.windy.com/api/point-forecast/v2"; //האנדפוינט למעשה
  //private KEY = "SaEeTq7ogsvMe8D3ppU8Ze73hgIb3E5r"; //האייפיאיי קיי האישי שלי

  classify(lon:number, lat:number):Observable<any>{
    let json = {
          "lat": lat,
          "lon": lon,
          "model": "gfs",
          "parameters": ["temp", "wind", "precip", "lclouds", "dewpoint", "rh", "pressure", "cape"],
          "levels": ["surface", "800h", "300h"],
          "key": "zYNSmQxcXjWGcbzCcIpReqrue4T449VF"
    }
    // let body = JSON.stringify(json);
    return this.http.post<any>(this.url,json).pipe(
      map(res => {
        return res;       
      })
    );      
  }

  /*
  // פונקציה שברגע שיש שגיאה מפנים אליה והיא מטפלת בשגיאות
  // אייצ'טיטיפי שיש בקונסקרטור יודעת מתי יש שגיאה ומתי אין, וכאשר כן יש שגיאה אז צריך לכתוב פונקצייה שהיא תלך אליה
  private handleError(res:HttpErrorResponse){ 
    console.log(res.error); //error שיצרנו יש שדה בשם res ראשית נרשום את השגיאות לקונסול בעזרת הפונקציה לוג. לאובייקט 
    return throwError (res.error || 'Server error'); //"לפחות שתיהיה ההודעה "סרבר ארור res נחזיר את השגיאה, ולמקרה שהגורם החיצוני לא טרח לכתוב את ההערה בתוך  
  }  
  
  

// פונקציה שמבצעת בפועל את ההתקשרות עם השרת - קריאת אייפיאיי
  searchWeatherData():Observable<Weather>{ //הקלט הוא שם של עיר, והפלט יהיה אובסרבל שהמידע שיהיה לו בפנים יהיה מהסוג נתונים של האינטרפייס ווטר
    return this.http.post<any>('https://api.windy.com/api/point-forecast/v2')
  }
  */
}
