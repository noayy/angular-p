import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Sea } from '../interfaces/sea';
import { SeaService } from '../sea.service';
import { AuthService } from './../auth.service';

interface City
{
  id: number;
  name: string;
  lat: number;
  lon: number;
}

@Component({
  selector: 'app-sea',
  templateUrl: './sea.component.html',
  styleUrls: ['./sea.component.css']
})
export class SeaComponent implements OnInit {

  constructor(private route:ActivatedRoute, private seaService:SeaService ,public authService:AuthService) { }

    //נגדיר את המשתנים ואת הסוג שלהם
    //city:string; 
    lat:number; 
    lon:number; 
    weatherData$:Observable<Sea>; //מוסכמה של אובסרבל שאם מוסיפים דולר למשתנה - זה אומר שנחבר למשתנה הזה אובסרבל. המידע שהאובסרבל יפלוט יהיה מסוג ווטר 
    hasError:Boolean = false; // הדיפולטיבי זה שאין שגיאה
    errorMessage:string; 
    seaData$ 

    //ערים
    cities: City[] = [{ id: 1, name: 'Jaffa', lon: 34.751, lat: 32.052 }, { id: 2, name: 'Hertzelia', lon: 34.843, lat: 32.166 }] //העיר האחרונה לא קיימת בכוונה
    city: City = this.cities[0];

   //משתנים למצב הים
    weather: any;
    windU: any;
    windV: any;
    temp: any;
    precip: any; 
    lclouds: any;
    dewpoint: any;
    rh: any;
    pressure :any;
    cape :any;

  
    classify(){
      this.seaData$ = this.seaService.classify(this.city.lon, this.city.lat);
      this.seaData$.subscribe(
        res => { 
          this.weather = res;
          this.windV = res['wind_u-surface']
          this.windU = res['wind_u-surface']
          this.temp = res['temp-surface']
          this.precip = res['past3hprecip-surface']
          this.lclouds = res['lclouds-surface']
          this.dewpoint = res['dewpoint-surface']
          this.rh = res['rh-surface']
          this.pressure = res['pressure-surface']
          this.cape = res['cape-surface']
        },
        error =>{ 
          console.log(error.message)
          this.hasError = true; 
          this.errorMessage = error.message;
        }
      )
    }



  ngOnInit(): void {
  }

}
