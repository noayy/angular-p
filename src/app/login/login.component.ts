import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService, private router:Router) { }


    //נגדיר משתנים
    hide = true; 
    email:string;
    password:string;
    errorMessage:string; 
    isError:boolean = false; 



    onSubmit(){ 
      this.authService.login(this.email, this.password)
      .then(res => {   
            console.log(res); 
            this.router.navigate(['/welcome']) 
          })
        .catch(err => { 
            console.log(err); 
            this.isError = true; 
            this.errorMessage = err.message; 
          }) 
      }



  ngOnInit(): void {
  }
}
