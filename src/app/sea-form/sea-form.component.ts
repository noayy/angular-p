import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sea-form',
  templateUrl: './sea-form.component.html',
  styleUrls: ['./sea-form.component.css']
})
export class SeaFormComponent implements OnInit {

  constructor(private router:Router) { }

    //נגדיר משתנים
  //נגדיר מערך מסוג ג'ייסון
  cities:Object[] = [{id:1,name:'Jaffa'},{id:2,name:'Hertzelia'},{id:3,name:'Ashdod'},{id:4,name:'BongaRRRR'}] //העיר האחרונה לא קיימת בכוונה
  city:string;


  onSubmit(){
    this.router.navigate(['/seaCondition',this.city]) 
  }



  ngOnInit(): void {
  }

}
