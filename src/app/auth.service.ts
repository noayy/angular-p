import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from './interfaces/user';
import { map } from 'rxjs/operators';

const GuideEmail = 'guide@guide.com';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  user: Observable<User | null>;
  isSecretary: Observable<boolean>;
  isGuide: Observable<boolean>;
  
  getUser():Observable<User | null>{
    return (this.user) ;
    
  }


  isSecretaryPipe(): Observable<boolean>
  {
    return this.user.pipe(
      map(user => user && user.email !== GuideEmail)
    );
  }

  isGuidePipe(): Observable<boolean>
  {
    return this.user.pipe(
      map(user => user && user.email == GuideEmail)
    );
  }

  

  SignUp(email:string, password:string){
    return this.afAuth
        .createUserWithEmailAndPassword(email,password)
  }

  Logout(){
    this.afAuth.signOut().then(
      res => {
        this.router.navigate(['/bye'])
      }
    );  
  }

  login(email:string, password:string){
    console.log(email)
    return this.afAuth
        .signInWithEmailAndPassword(email,password)
  }

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
          this.user = this.afAuth.authState;
          this.isSecretary = this.isSecretaryPipe();
          this.isGuide = this.isGuidePipe();
          console.log("auth service constructr worked");
    }
}
