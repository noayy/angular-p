import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { WeatherApiService } from '../weather-api.service';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  city:string; 
  temperature:number; 
  image:string; 
  lon:number; 
  lat:number;
  country:string; 
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;

  constructor(private route:ActivatedRoute, private weatherService:WeatherApiService, public authService:AuthService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city; 
    this.weatherData$ = this.weatherService.searchWeatherData(this.city); 
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lon = data.lon;
        this.lat = data.lat;  
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )
  }

}
