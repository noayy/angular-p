import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { ApiComponent } from './api/api.component';
import { SeaComponent } from './sea/sea.component';
import { SeaFormComponent } from './sea-form/sea-form.component';
import { Api2Component } from './api2/api2.component';
import { TableCustomerComponent } from './table-customer/table-customer.component';
import { LeaveCustomerComponent } from './leave-customer/leave-customer.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'bye', component: ByeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: SignUpComponent},  
  { path: 'customers', component: TableCustomerComponent}, 
  { path: 'addCustomer', component: CustomerFormComponent},
  { path: 'apiweatherJaffa/:city', component: ApiComponent},
  { path: 'apiweatherHerzliya/:city', component: Api2Component},
  { path: 'seaCondition', component: SeaComponent }, 
  { path: 'city', component: SeaFormComponent },
  { path: 'leavecustomers', component: LeaveCustomerComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
