import { AuthService } from './../auth.service';
import { TableCustomerService } from './../table-customer.service';
import { ClassifyService } from './../classify.service';
import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '../interfaces/customer';


@Component({
  selector: 'app-table-customer',
  templateUrl: './table-customer.component.html',
  styleUrls: ['./table-customer.component.css']
})
export class TableCustomerComponent implements OnInit {


  customers=[];
  displayedColumns: string[] = ['name','phone','email','more_than_1_card','predict','edit','delete'];
  customers$;
  new;
  resultPredict;

  rowToEdit:number = -1; //אתחול אינדקס השורה עם מינוס 1 כדי שלא נהיה במצב של עריכה בשום שורה
  customerToEdit:Customer = {
  id:null,
  age:null,
  more_than_1_card:null,
  student:null,
  main_city_sail:null,
  Utilizing_card_1:null,
  Utilizing_card_2:null,
  Utilizing_card_3:null,
  Utilizing_card_4:null,
  perc_private:null,
   perc_proff:null,
   name:null,
   email:null,
   phone:null
  };

  addBookFormOpen = false;

  lastCustomerArrived //דפדוף
  firstCustomerArrived;//דפדוף

  @Input() name:string;
  @Input() email:string;
  @Input() phone:string;

  moveToEditState(index){ //הפונקציה מאתרת את מספר השורה שבה הפריט שנעדכן נמצא
    this.customerToEdit.age = this.customers[index].age;
    this.customerToEdit.more_than_1_card = this.customers[index].more_than_1_card;
    // this.customerToEdit.student = this.customers[index].student;
    // this.customerToEdit.main_city_sail = this.customers[index].main_city_sail;
    // this.customerToEdit.Utilizing_card_1 = this.customers[index].Utilizing_card_1;
    // this.customerToEdit.Utilizing_card_2 = this.customers[index].Utilizing_card_2;
    // this.customerToEdit.Utilizing_card_3 = this.customers[index].Utilizing_card_3;
    // this.customerToEdit.Utilizing_card_4 = this.customers[index].Utilizing_card_4;
    // this.customerToEdit.perc_private = this.customers[index].perc_private;
    // this.customerToEdit.perc_proff = this.customers[index].perc_proff;
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.email = this.customers[index].email;
    this.customerToEdit.phone = this.customers[index].phone;
    this.rowToEdit = index; 
  }

  constructor(private TableCustomerService:TableCustomerService, private ClassifyService:ClassifyService, public authService:AuthService) { }


  // updateCustomer(){
  //   let id = this.customers[this.rowToEdit].id;
  //   this.TableCustomerService.editCustomer(id, this.customerToEdit.age, this.customerToEdit.more_than_1_card, 
  //     this.customerToEdit.student,this.customerToEdit.main_city_sail, this.customerToEdit.Utilizing_card_1,
  //     this.customerToEdit.Utilizing_card_2,this.customerToEdit.Utilizing_card_3,this.customerToEdit.Utilizing_card_4,
  //     this.customerToEdit.perc_private, this.customerToEdit.perc_proff, this.customerToEdit.name,
  //     this.customerToEdit.email,this.customerToEdit.phone);
  //   this.rowToEdit = null;
  // }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.TableCustomerService.editCustomer(id,this.customerToEdit.more_than_1_card, this.customerToEdit.name,
      this.customerToEdit.email,this.customerToEdit.phone);
    this.rowToEdit = null;
  }

  predict(){
    this.ClassifyService.classify(this.customers).subscribe(
                   (res:number[])=> {
                     console.log(res);
                    //  if (res > 0.5){
                    //    customer.predict = 'will not leave';
                    //  }
                    //  if (res < 0.5) {
                    //    customer.predict = 'will leave';
                    //  }
                    for (let i=0;i<res.length; i++){
                      const customerPrediction = res[i];
                      const predict = customerPrediction>0.5?'will not leave':'will leave';
                      const customer:Customer = this.customers[i];
                      const {id} = customer;
                      this.TableCustomerService.updateCustomer(id, predict);

                    }
                    }
                 )         
  }


  deleteCustomer(index){
    let id = this.customers[index].id;
    this.TableCustomerService.deleteCustomer(id);
  }


  ngOnInit(): void {

          this.customers$ = this.TableCustomerService.getCustomers(null);
          this.customers$.subscribe(
            docs => {  
              this.lastCustomerArrived = docs[docs.length-1].payload.doc;       
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();

                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }   
            //  console.log(this.customers);
                     
            }
            
          )
      }

  //דפדוף
nextPage(){
  this.customers$ = this.TableCustomerService.getCustomers(this.lastCustomerArrived); 
  this.customers$.subscribe( 
    docs => { 
      this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
      this.firstCustomerArrived = docs[0].payload.doc; 
      this.customers = []; 
      for(let document of docs){ 
        const customer:Customer = document.payload.doc.data(); 
        customer.id = document.payload.doc.id; 
        this.customers.push(customer); 
      } 
    }
    )
    }



//דפדוף
previewsPage(){
this.customers$ = this.TableCustomerService.getCustomers2(this.firstCustomerArrived); 
this.customers$.subscribe( 
docs => { 
  this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
  this.firstCustomerArrived = docs[0].payload.doc; 
  this.customers = []; 
  for(let document of docs){ 
    const customer:Customer = document.payload.doc.data(); 
    customer.id = document.payload.doc.id; 
    this.customers.push(customer); 
  } 
}
)
}

}
