// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAVyizRR9OycSAzuKO4_M7Q0fFKsrQsevE",
    authDomain: "project-final-db3c1.firebaseapp.com",
    projectId: "project-final-db3c1",
    storageBucket: "project-final-db3c1.appspot.com",
    messagingSenderId: "570574418616",
    appId: "1:570574418616:web:b05d28fb435836adc23a4d"
  }
};
//


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
